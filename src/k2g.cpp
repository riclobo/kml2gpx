// Swap lines and changes tags to facilitate kml to gpx conversion 
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

// Uncomment for testing (delays the reading)
//#include <time.h>

using namespace std;

//
// Usage k2g inputfile.kml [outputfile.gpx]
// If output file is ommited a file with the same name as input is created
//
int main(int argc, char *argv[])
{
  // Variables
  string line;
  vector<string> when, coord;
  size_t p1, p2;
  unsigned nline;
  int TrackOpen = 0;

  cout << endl;
  
  // Verify number of arguments
  if (argc != 2 && argc != 3)
  {
    cout << "Usage" << endl << endl
         << "  $ k2g input_file.kml [output_file.gpx]" << endl << endl
         << "  If output file name is ommited, a file with the" << endl
         << "  same name as the input is created and overwrites" << endl
         << "  any pre-existing file without confirmation" << endl << endl;

    return 0;
  }

  // Open input file and check for its existence
  ifstream filein(argv[1]);

  if (!filein)
  {
    cout << "File " << argv[1] << " not found!" << endl << endl;
    return 0;
  }

// Uncomment for testing (delays the reading)
//  struct timespec req = {0};
//  req.tv_sec = 0;
//  req.tv_nsec = 1000000L;      // 1 ms

  // Parse input file and look for <when> and <gx:coord> lines
  cout << "Parsing " << argv[1] << endl;
  nline = 0;
  while (!filein.eof())
  {
    getline(filein, line);
    nline++;

    cout << "Line: " << nline << '\r' << flush;

// Uncomment for testing (delays the reading)
//    nanosleep(&req, (struct timespec *)NULL);   
    
    if (line.find("<gx:Track>")) TrackOpen++;    // Next lines are in a track
    if (line.find("</gx:Track>")) TrackOpen--;   // Closes a track

    if (!TrackOpen) continue;  // No track opened. Moves to next line
    
    if ((p1 = line.find("<when>")) != string::npos)   // Found <when> tag
    {
      p1 += 6;    // Sets p1 at the end of tag
      if ((p2 = line.find("</when>")) == string::npos)  // Looks for </when> tag
      {
        cout << endl << "Error in <when>...</when> tag at line " << nline << endl << endl;
        return 0;
      }
      when.push_back(line.substr(p1,p2-p1));
    }
    else if ((p1 = line.find("<gx:coord>")) != string::npos)  // Found <gx:coord> tag
    {
      p1 += 10;    // Sets p1 at the end of tag
      if ((p2 = line.find("</gx:coord>")) == string::npos)  // Looks for </when> tag
      {
        cout << endl << "Error in <gx:coord>...</gx:coord> tag at line " << nline << endl << endl;
        return 0;
      }
      coord.push_back(line.substr(p1,p2-p1));
    }
  }

  filein.close();

  cout << endl
       << "Found " << when.size() << " time stamps and "
       << coord.size() << " coordinates" << endl;

  // Some error checks
  if (when.size() != coord.size())
  {
    cout << "Error: Number of time stamps and coordinates are different (could also be an unmatched <gx:Track> tag)." << endl << endl;
    return 0;
  }

  if (TrackOpen)
  {
    cout << "Error: Unmatched <gx:Track> and </gx:Track> tags." << endl << endl;
    return 0;
  }

  // Save output file
  if (argc == 3)
    line = argv[2];
  else
  {
    line = argv[1];
    line += ".gpx";
  }

  cout << "Saving " << line << endl;

  // Opens file for output
  ofstream fileout(line.c_str());
  if (!fileout)
  {
    cout << "Could not open " << line << endl << endl;
    return 0;
  }

  // Save contents -- Header
  fileout << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl
          << "<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" creator=\"kml2gpx v0.0\">" << endl
          << "<metadata>" << endl
          << "<name>" << argv[1] << "</name>" << endl
          << "<time>" << when[0] << "</time>" << endl
          << "</metadata>" << endl;
          
  // Save contents -- Tracks
  fileout << "<trk>" << endl
          << "<name>" << argv[1] << "</name>" << endl
          << "<trkseg>" << endl;
  
  for (size_t i = 0; i < when.size(); i++)
  {
    p1 = coord[i].find_first_of(" ");
    p2 = coord[i].find_last_of(" ");

    if (p1 == p2 || p1 == string::npos || p2 == string::npos)
    {
      cout << endl << "Skipping item " << i << " (Error in coordinates format)" << endl;
      continue;
    }
    
    fileout << "<trkpt ";           // Open trackpoint
    fileout << "lat=\"" << coord[i].substr(p1+1, p2-p1-1) << "\" ";   // Latitude
    fileout << "lon=\"" << coord[i].substr(0, p1) << "\">";           // Longitude
    fileout << "<ele>" << coord[i].substr(p2+1) << "</ele>";          // Elevation
    fileout << "<time>" << when[i] << "</time>";                      // Time
    fileout << "</trkpt>" << endl;  // Close trackpoint
  }
  
  fileout << "</trkseg>" << endl
          << "</trk>" << endl
          << "</gpx>" << endl;

  cout << "Done!" << endl << endl;

  return 0;
}
